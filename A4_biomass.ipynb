{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "%run imports.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Isolume and mixed layer depths"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We do not convert chl-a fluorescence and bbp into carbon biomass. Note that as commonly used parameterizations for biomass are linear in chl-a fluorescence and bbp, respectively, the choice of parameter values will not affect computed biomass rate of changes (\"r\").\n",
    "\n",
    "The result (`ds`) consists of four variables:\n",
    "An estimate of biomass and biomass rate of change for each date, float, variable (bbp or chl-a fluorescence) and averaging method (mean, integral), and the corresponding MLDs and MLD changes for each date and float.\n",
    "\n",
    "In the end, we follow a logic modified from Boss et al.'s (2011) Eqn. (4) and select the appropriate vertical averaging method (integrating or arithmetic mean) when inferring the rate of mixed-layer biomass change."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = xr.load_dataset('nb_tmp_data/FLOATS_by_profile_derived.nc')\n",
    "\n",
    "# rate of change of mixed layer depth\n",
    "ds['dmld_dt'] = (\n",
    "    ds.mld.diff(dim='profile') \n",
    "    / (ds.Date.diff(dim='profile') / np.timedelta64(1, 'D'))\n",
    ").interp_like(ds)\n",
    "\n",
    "ds = ds.merge(xr.load_dataset('nb_tmp_data/LIGHT_isolumes.nc'), compat='no_conflicts').sel(Float=['012b', '016b', '017b', '020b'])\n",
    "\n",
    "# np.maximum propagates NaNs. \n",
    "ds['integration_depth'] = xr.where(\n",
    "    ~(ds.mld.isnull() & ds.isolume.isnull()), # if not both are zero\n",
    "    np.maximum( # then take the maximum of isolume and MLDs\n",
    "        ds.mld.fillna(-np.inf).interpolate_na(dim='profile'), \n",
    "        ds.isolume.fillna(-np.inf).interpolate_na(dim='profile')\n",
    "    ),\n",
    "    np.nan # else nan\n",
    ").where( # finally, use the max of the two only when mld is not NaN\n",
    "    ~ds.mld.isnull(), \n",
    "    np.nan #otherwise set to nan, too\n",
    ")\n",
    "\n",
    "ds.to_netcdf('nb_tmp_data/FLOATS_by_profile_derived_surfacelayer.nc')\n",
    "df = ds.drop_dims('Depth').to_dataframe().reset_index().dropna(subset=['Date'])\n",
    "df.to_csv('nb_tmp_data/FLOATS_surfacelayer.csv', index=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dims = dict(\n",
    "    integration_depth=hv.Dimension('depth', label='A: Surface layer depth', unit='m'),\n",
    "    mld=hv.Dimension('depth', label='B: Mixed Layer Depth', unit='m'),\n",
    "    isolume=hv.Dimension('depth', label='C: 0.4 mol m-2 d-1 isolume depth', unit='m'),\n",
    ")\n",
    "\n",
    "l = [df.hvplot.scatter('Date', var, by='Float', label=dims[var].pprint_label)\n",
    "     .map(lambda e: hv.Scatter(e, vdims=[var, 'profile']), hv.Scatter)\n",
    "     for var in ['integration_depth', 'mld', 'isolume']]\n",
    "l = hv.Layout(l).cols(1).redim(**dims).redim.range(depth=(-1, 110))\n",
    "l = l.opts(\n",
    "    opts.Scatter(width=700, frame_height=200, tools=['hover'], padding=.1, size=5, ylabel='', xlabel='',\n",
    "                 color=hv.Cycle(['#A65B72', '#558CAC', '#58AB70', '#E2A34B']),\n",
    "                 hooks=[lambda plot, element: minor_datetime_grid(plot.state)]),\n",
    "    opts.NdOverlay(legend_position='right', show_grid=True)\n",
    ")\n",
    "hv.save(l.opts(toolbar=None, clone=True), 'nb_fig/timeseries_surfacelayer.png')\n",
    "l"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Is ML biomass concentration significantly larger than below?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add_biomass_ratio_and_significance(ds):\n",
    "    vv = ['Corbbp', 'CorFChla']\n",
    "\n",
    "    ds = ds.sel(Depth=slice(0, 200))\n",
    "\n",
    "    def mixed_layer_base_difference(ds):\n",
    "        for v in vv:\n",
    "            da_ml = ds[v].where((ds.Depth<=ds.mld)).mean(dim='Depth')\n",
    "            da_below = ds[v].where((ds.Depth>=ds.mld) & (ds.Depth<=ds.mld+15)).mean(dim='Depth')\n",
    "            ds[v+'_ratio'] = da_ml/da_below\n",
    "\n",
    "    def ttest_xr(a, b):\n",
    "        if not all(a.isnull()) and not all(b.isnull()):\n",
    "            a, b =a.values, b.values\n",
    "            a, b = a[~np.isnan(a)], b[~np.isnan(b)]\n",
    "            res = ttest_ind(a, b, axis=None, equal_var=False, nan_policy='omit')\n",
    "            return (res.statistic > 0) and (res.pvalue/2 < 0.05) # one-sided\n",
    "        else:\n",
    "            return np.nan\n",
    "\n",
    "    def mixed_layer_base_difference_sig(ds):\n",
    "        for v in vv:\n",
    "            da_ml = ds[v].where((ds.Depth<=ds.mld))\n",
    "            da_below = ds[v].where((ds.Depth>=ds.mld) & (ds.Depth<=ds.mld+15))\n",
    "            ds[v+'_larger'] = apply_1d([da_ml, da_below], ttest_xr, dim='Depth')\n",
    "\n",
    "    mixed_layer_base_difference(ds)\n",
    "    mixed_layer_base_difference_sig(ds)\n",
    "    return ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Mean and integrated biomasses"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def surface_layer_biomass(ds):\n",
    "    \"\"\"\n",
    "    Calculates the mean and vertical integral of surface layer biomass from chla and bbp\n",
    "    \"\"\"\n",
    "    \n",
    "    from_var = ['CorFChla', 'Corbbp']\n",
    "    avg = ['int', 'mean']\n",
    "\n",
    "    da_bm_list = []\n",
    "    for v in from_var:\n",
    "        biomass_proxy = ds[v]\n",
    "        avg_biomass = xr.where(\n",
    "            ~ds.integration_depth.isnull(), # only keep biomass where integration was not to zero\n",
    "            (\n",
    "                biomass_proxy.where(ds.Depth<=ds.integration_depth)\n",
    "                .reduce(np.nanmean, dim='Depth') # the actual mean/integral is happening here\n",
    "            ),\n",
    "            np.nan\n",
    "        ).assign_coords(avg='mean', from_var=v)\n",
    "\n",
    "        int_biomass = (ds.integration_depth * avg_biomass).assign_coords(avg='int', from_var=v)\n",
    "\n",
    "        da_bm_list.append(avg_biomass)\n",
    "        da_bm_list.append(int_biomass)\n",
    "\n",
    "    da_bm = xr.concat(\n",
    "        [xr.concat([d for d in da_bm_list if avg[i] in d.avg], 'from_var') for i in range(2)], \n",
    "        dim='avg'\n",
    "    )\n",
    "\n",
    "    ds_bm = da_bm.rename('biomass').to_dataset()\n",
    "\n",
    "    ds_bm = ds_bm.where(ds_bm.biomass>=0)\n",
    "\n",
    "    ds_bm['Date'] = ds.Date\n",
    "    ds_bm['isolume'] = ds.isolume\n",
    "    ds_bm['isolume_0.4'] = ds['isolume_0.4']\n",
    "    ds_bm['isolume_0.1'] = ds['isolume_0.1']\n",
    "    ds_bm['isolume_0.05'] = ds['isolume_0.05']\n",
    "    ds_bm['mld'] = ds.mld\n",
    "\n",
    "    ds_bm['biomass_sig_larger'] = xr.concat([ds.Corbbp_larger.assign_coords(from_var='Corbbp'), \n",
    "                                            ds.CorFChla_larger.assign_coords(from_var='CorFChla')], \n",
    "                                            dim='from_var')#.rename('biomass_sig_larger')\n",
    "    \n",
    "    # take over variables from input dataset\n",
    "    ds_bm['integration_depth'] = ds.integration_depth\n",
    "    \n",
    "    ds_bm['from_var'] = ['chla', 'bbp']\n",
    "\n",
    "    ds_bm['background'] = xr.concat([\n",
    "        ds.chla_background.assign_coords(from_var='chla'),\n",
    "        ds.bbp_background.assign_coords(from_var='bbp')\n",
    "    ], 'from_var')\n",
    "    \n",
    "    ds_bm['background_std'] = xr.concat([\n",
    "        ds.chla_background_std.assign_coords(from_var='chla'),\n",
    "        ds.bbp_background_std.assign_coords(from_var='bbp')\n",
    "    ], 'from_var')\n",
    "\n",
    "    # finalize output\n",
    "    ds = ds_bm.copy()\n",
    "\n",
    "    ds['biomass_mean'] = ds.biomass.sel(avg='mean')\n",
    "    ds['biomass_int'] = ds.biomass.sel(avg='int')\n",
    "\n",
    "    return ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Significance of measurements of so small biomasses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A crucial issue is how significant the growth rates can be when inferred from such small biomasses."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dark drift of chlorophyll a and bbp sensors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Floats = ['012b', '016b', '017b', '020b']\n",
    "df = xr.load_dataset('nb_tmp_data/FLOATS_by_date_derived.nc').sel(Depth=slice(750, 800)).mean('Depth').to_dataframe().dropna(subset=['Year', 'Corbbp', 'CorFChla']).reset_index()\n",
    "df = df.loc[df.Float.isin(Floats)]\n",
    "\n",
    "df.groupby('Float')[['Corbbp', 'CorFChla']].mean().rename(columns={'CorFChla': 'chla', 'Corbbp': 'bbp'}).style.format('{:.2e}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here they are averaged over all floats:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.groupby('Float')[['Corbbp', 'CorFChla']].mean().rename(columns={'CorFChla': 'chla', 'Corbbp': 'bbp'}).mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let's look at the trends of these values and the standard deviations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df['seq_doy'] = (df.Year-2017)*365 + df.Day\n",
    "\n",
    "def trends(df):\n",
    "    df = df.copy()\n",
    "    df = df.dropna(subset=['Year', 'Corbbp', 'CorFChla'])\n",
    "    model = sm.OLS.from_formula('CorFChla ~ seq_doy', df)\n",
    "    results = model.fit()\n",
    "    # print(results.summary())\n",
    "    print('pvalue', results.pvalues['seq_doy'])\n",
    "\n",
    "    a, b = df.CorFChla.std(), results.params.loc['seq_doy'] * 365\n",
    "\n",
    "    model = sm.OLS.from_formula('Corbbp ~ seq_doy', df)\n",
    "    results = model.fit()\n",
    "    results.params\n",
    "    # print(results.summary())\n",
    "    print('pvalue', results.pvalues['seq_doy'])\n",
    "    \n",
    "    c, d = df.Corbbp.std(), results.params.loc['seq_doy'] * 365\n",
    "\n",
    "    return a, c, b, d\n",
    "\n",
    "results = []\n",
    "for f in Floats:\n",
    "    print('+++++++++++++++++++++++++++')\n",
    "    print('Float', f)\n",
    "    results.append(trends(df.loc[df.Float==f]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.DataFrame(results, columns=pd.MultiIndex.from_product([('stdev', 'Change/year'), ('chla', 'bbp')]), index=Floats)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These numbers, averaged over all floats, are reported in the SupMat:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df['stdev'].mean()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df['Change/year'].mean()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(df['Change/year']/df.stdev).abs()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here they are float-by-float:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df['stdev'].style.format('{:.2e}')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df['Change/year'].style.format('{:.2e}')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(df['Change/year']/df.stdev)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(df['Change/year']/df.stdev).abs().mean()#.style.format('{:.2e}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## bbp and chla background levels"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, explore the \"deep\" (750-800 m) background in chlorophyll fluorescence and bbp:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = xr.load_dataset('nb_tmp_data/FLOATS_by_date_derived.nc').sel(Depth=slice(750, 800)).median('Depth')\n",
    "\n",
    "df = ds.to_dataframe().reset_index().dropna(subset=['Corbbp', 'profile'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.Corbbp.hvplot.hist(bins=100)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.hvplot.scatter('Date', 'Corbbp')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.groupby('Float').Corbbp.describe()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.groupby('Float').CorFChla.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us put this into a function to be run later:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add_biomass_background(ds):\n",
    "    df = (\n",
    "        xr.load_dataset('nb_tmp_data/FLOATS_by_date_derived.nc')\n",
    "        .sel(Float=ds.Float)\n",
    "        .sel(Depth=slice(750, 800)).median('Depth')\n",
    "        .to_dataframe().reset_index().dropna(subset=['Corbbp', 'profile'])\n",
    "    )\n",
    "\n",
    "    ds['chla_background'] = ('Float', df.groupby('Float').CorFChla.mean())\n",
    "    ds['chla_background_std'] = ('Float', df.groupby('Float').CorFChla.std())\n",
    "\n",
    "    ds['bbp_background'] = ('Float', df.groupby('Float').Corbbp.mean())\n",
    "    ds['bbp_background_std'] = ('Float', df.groupby('Float').Corbbp.std())\n",
    "    \n",
    "    return ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ice algae sloughing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Can sloughing of ice algae have impacted our chla/bbp signals?\n",
    "\n",
    "Let's go look in Leu et al 2015 (Figure 3).\n",
    "\n",
    "Mixing a concentration of 1 mg chla/m2 over a mixed layer of 50 m e.g. leads to this much concentration in mg chla/m3 (i.e. very little):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "1/50"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the North Water polynya, significant biomass started growing from day 100, i.e. not before mid-April"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.Timestamp('2017-4-1').dayofyear"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Apply above calculations and save output"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = xr.load_dataset('nb_tmp_data/FLOATS_by_profile_derived_surfacelayer.nc')\n",
    "ds = add_biomass_background(ds)\n",
    "ds = ds.merge(add_biomass_ratio_and_significance(ds))\n",
    "\n",
    "ds = surface_layer_biomass(ds)\n",
    "\n",
    "ds.to_netcdf('nb_tmp_data/FLOATS_biomass.nc')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Some notes ( Biomass and biomass accumulation rates)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Behrenfeld2005:\n",
    "\n",
    "> It is well established from decades of laboratory studies that phytoplankton Chl:C ratios decrease from low to high light [e.g., Geider, 1987; Sakshaug et al., 1989; Geider et al., 1998; MacIntyre et al., 2002; Behrenfeld et al., 2002]\n",
    "\n",
    "Behrenfeld & Boss 2006 Fig 3C:\n",
    "\n",
    "> Fluo:cp is highest at low light\n",
    "\n",
    "Kvernvik et al:\n",
    "\n",
    "> Like a typical acclimation response to the increased irradiance (compared to in situ val- ues), rPSII started to decrease, most likely due to the declining Chl a content (data not shown), thereby providing a mechanism to protect phyto- plankton against damage by absorption of excess light (Falkowski and Owens 1980)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Behrenfeld2005:\n",
    "\n",
    "> In five moderately productive regions, temporally offset seasonal cycles of Chl and C biomass are found, with the rise in Chl preceding the rise in C (Figure 2d). We interpret this pattern as a seasonal cycle where initial cell ‘‘greening’’ is followed by increased growth and biomass, and later culminates in nutrient- and light-dependent reduc- tions in pigmentation and growth.  ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "http://biogeochemical-argo.org/cloud/document/meetings/admt/18/admt18-workshop-6-bgc-argo-d1_13-eco_barnard.pdf\n",
    "\n",
    "bbp:\n",
    "https://archimer.ifremer.fr/doc/00283/39459/56146.pdf\n",
    "\n",
    "chla:\n",
    "https://archimer.ifremer.fr/doc/00243/35385/60181.pdf\n",
    "\n",
    "http://www.comm-tec.com/Prods/mfgs/Wetlabs/Manuals/Eco-fluo_manual.pdf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Differences between successive profiles"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For completeness' sake, let us quickly determine the relative size of the differences between mean biomasses measured in winter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Floats = ['012b', '016b', '017b', '020b']\n",
    "ds = xr.load_dataset('nb_tmp_data/FLOATS_biomass.nc').sel(Float=Floats, avg='mean')\n",
    "\n",
    "df = ds.biomass.diff('profile').to_dataset().merge(ds.Date).to_dataframe().dropna().reset_index().rename(columns={'biomass': 'biomass_diff'})\n",
    "\n",
    "diff = df.hvplot.scatter('Date', 'biomass_diff', groupby='from_var') * hv.HLine(0)\n",
    "\n",
    "biomass = ds[['biomass', 'Date']].to_dataframe().dropna().hvplot.scatter('Date', 'biomass', groupby='from_var')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(diff+biomass).cols(1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "0.0037 / np.sqrt(300)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can see that occasionally, differences between successive surface layer mean chl-a values are on the order of only a few digital counts (0.0037 mg m-3). What does such a difference entail in terms of specific growth rates (units d-1)? E.g.:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "0.0037/0.02"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Yet as we show in the Supplementary Material, as each of those surface layer mean values is composed of hundreds of individual measurements, the standard error is approximately a factor of 10 better than the digital count."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  },
  "toc-autonumbering": true
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
