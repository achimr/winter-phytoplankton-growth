# panel apparently needs to be imported first
import panel as pn
pn.extension()

#BASIC

from collections import OrderedDict
from contextlib import contextmanager
import os
from copy import deepcopy
import itertools
from functools import reduce, partial
import datetime as dt
import warnings
import json

# MISC

from pysolar.solar import get_altitude
import gsw

# DATA

from scipy.stats import ttest_1samp, ttest_ind, kruskal
import statsmodels.stats.api as sms
import statsmodels.formula.api as smf
import statsmodels.api as sm
from pygam import LinearGAM
import pandas as pd
import geopandas as gpd
import numpy as np
import xarray as xr

# GEO

from fiona.crs import from_epsg

from scipy.spatial import cKDTree

import cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature

import xesmf
import shapely
from shapely.ops import nearest_points
from shapely.geometry import Point, MultiPoint, box, LinearRing, LineString, MultiLineString, Polygon

## VIZ

import cmocean
import colorcet

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import (
    dates as mdates,
    patches as mpatches,
    transforms as mtransforms
)
# import seaborn as sns
# plt.style.use('styles/oldschool.mplstyle')

from bokeh.models import WMTSTileSource
from bokeh.io import show, export_png, curdoc

import holoviews as hv
hv.extension('bokeh', 'matplotlib')

from holoviews import dim, opts, Options, Dimension, Options, Cycle
from holoviews.core.io import Pickler
from holoviews.operation.datashader import datashade, rasterize, regrid, aggregate, dynspread
from holoviews.operation.stats import univariate_kde
from holoviews.plotting.links import DataLink

import datashader as dsh
import datashader.reductions as dsr

import hvplot.pandas
import hvplot.xarray

import param

extents=(-90, 65, -45, 78)
bathy = gpd.read_file('/Users/doppler/database/IBCAO/IBCAO_1min.shp').set_index('contour').geometry
# e.g. bathy200 = bathy.loc[200]
bathy = bathy.intersection(shapely.geometry.box(*extents))
bathy.crs = from_epsg(4326)

default_options = []
for e in hv.Store.registry['bokeh']:
    if issubclass(e, hv.element.Chart) or issubclass(e, hv.element.Geometry):
        default_options.append(hv.Options(e.name, padding=.05))
hv.opts.defaults(*default_options)

# BOKEH STYLING

from bokeh.themes import Theme
bokeh_theme_dict = {
    'Figure' : {
#         'background_fill_color': '#2F2F2F',
#         'border_fill_color': '#2F2F2F',
        'outline_line_color': 'black',
        'outline_line_alpha': 0.5,
        'outline_line_width': 2,
    },
    'Axis' : {
        'axis_line_width': 2,
        'major_tick_line_width': 2,
        'minor_tick_line_width': 2,
        'major_tick_in': 0,
        'major_tick_out': 8,
        'minor_tick_out': 0,
        'axis_label_standoff': 12,
        'axis_label_text_color': 'black',
        'axis_label_text_font_style': 'normal',
        'axis_label_text_font_size': '15pt',
        'major_label_text_font_size': '13pt',
        'major_label_standoff': 6,
    },
    'BasicTicker': {
        'num_minor_ticks': 0
    },
    # 'LogAxis' : {
    #     'minor_tick_out': 4,
    # },
    'DatetimeTickFormatter': {
        'months': '%b',
        'days': '%b-%d',
    },
    'CategoricalAxis' : {
        'separator_line_width': 0,
    },
    'Legend': {
        'label_text_font_size': '13pt',
        'border_line_alpha': 0,
        'background_fill_alpha': 0.8,
    },
    'Grid': {
        'grid_line_alpha': 0.5,
        'grid_line_dash': [6, 4],
        'grid_line_alpha': .9,
        # 'minor_grid_line_color': 'grey',
        # 'minor_grid_line_alpha': 0.5,
        # 'minor_grid_line_dash': [4, 6],
    },
    'Title': {
        'text_font_size': '15pt',
    },
    'ColorBar': {
        'major_label_text_font_size': '14pt',
        'title_text_align': 'right',
        'title_text_font_style': 'normal',
        'title_text_font_size': '13pt',
        'major_tick_in': 0,
        'major_tick_out': 6,
        'major_tick_line_width': 1,
        'title_text_align': 'right',
        'label_standoff': 4,
    },
}
theme = Theme(json={'attrs' : bokeh_theme_dict})

hv.renderer('bokeh').theme = theme

# OWN UTILS

# `get_unique` makes collapsing xarray.Dataset dimensions easier
from utils.xarray import get_unique, apply_1d, ols
from utils.pandas import (with_df_as_numeric, date_range_with_fix,
                          interp_series_on_index, df_to_gdf)
from utils.holoviews import get_all_data
from utils.mpl import align_xaxis_extents, cmap_to_list
from utils.holoviews.export_multisvg import save_bokeh_svg_multipanel
from utils.holoviews.twinaxis_bokeh import twinaxis_hook


# --- LIGHT
class LightAttenuation:
    """
    Describe relative magnitude between irradiance at various sun angles, even below horizon.

    Supposing that twilight irradiance goes as `exp(alpha*k)` and daytime irradiance goes
    as `sin(alpha)`, requiring them to join smoothly (first derivatives agree) means they
    join at alphac, and the ratio between them is phi, as follows.

    k (attenuation_factor, modulo the conversion degree to radian) is supplied by the user.
    k defaults to the value read off of the reference's Fig. 5.

    References
    ----------
    [1] Ekstrom, P. Error measures for template-fit geolocation based on light.
    Deep Sea Research Part II: Topical Studies in Oceanography 54, 392–403 (2007).
    """
    def __init__(self, attenuation_factor=np.log(2.37)):
        self.attenuation_factor = attenuation_factor
        self.alphac = np.arctan(np.pi/180/self.attenuation_factor) * 180/np.pi
        self.phi = np.sin(self.alphac *np.pi/180) / np.exp(self.alphac * self.attenuation_factor)

    def __call__(self, alpha):
        input_type = type(alpha)

        weighting_factor = np.where(
            alpha > self.alphac,
            np.sin(alpha *np.pi/180), # daylight
            self.phi * np.exp(alpha * self.attenuation_factor), # twilight/night
        )
        if input_type == xr.DataArray:
            return xr.ones_like(alpha) * weighting_factor
        else:
            return weighting_factor

weight_twilight = LightAttenuation(np.log(2.37)) # ekstrom2007error Fig. 5

class DoubleLogTransform:
    """
    Defines a double log transform with a linear transition. The log transforms on either side
    can be squished independently by specifiying an alpha ~= 1.
    """
    def __init__(self, y0=1e-3, alpha=1):
        self.y0 = y0
        self.alpha = alpha
        self.slope = 1 / (y0 * np.log(10))
        self.yoff = np.log10(y0) - 1/np.log(10)
        self.y0_neg = alpha / (self.slope * np.log(10))

    def linear_regime(self, y):
        return self.yoff + y * self.slope

    def __call__(self, y):
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            y0, y0_neg, alpha = self.y0, self.y0_neg, self.alpha
            result = np.where(
                y > y0,
                np.log10(y), # above y0: log transform
                np.where(
                    y <= -y0_neg,
                    # below -y0_neg: log transform squeezed by alpha
                    - np.log10((-y)**alpha) + np.log10(y0_neg**alpha) + self.linear_regime(-y0_neg),
                    self.linear_regime(y) # in between: linear
                )
            )
            if not result.shape:
                result = float(np.real(result))
            return result


# MATPLOTLIB hooks
def set_subplot_titles(fig):
    """
    Turn yaxis labels into plot subtitles.
    """
    has_subplots = len(fig.axes)>=2
    for k, ax in enumerate(fig.axes):
        letter = chr(k+97).upper()+': ' if has_subplots else ''
        ax.set_title(letter+ax.get_ylabel(), loc='left', pad=5)
        ax.set_ylabel('')
    return fig

time_ticks = pd.date_range('2017-7-1', '2018-9-1', freq='2MS').to_pydatetime()
time_ticks = mdates.date2num(time_ticks)

from bokeh.models import DatetimeTicker, Grid, FixedTicker, DatetimeAxis, BasicTicker

def minor_datetime_grid(p):
    for subplotgrid in p.select({'type': Grid}):#, 'ticker': {'type': BasicTicker}}):
        subplotgrid.grid_line_alpha = 0
        subplotgrid.minor_grid_line_alpha = 0

    major_datetime_ticks = pd.date_range('2017-9-1', '2019-9-1', freq='4MS').to_series().astype(int) / 1e6
    minor_datetime_ticks = pd.date_range('2017-7-1', '2019-11-1', freq='1MS').to_series().astype(int) / 1e6
    ticker = FixedTicker(ticks=major_datetime_ticks, minor_ticks=list(set(minor_datetime_ticks)-set(major_datetime_ticks)))

    # subplotaxis = p.select({'type': DatetimeAxis})
    for subplotaxis in p.select({'type': DatetimeAxis}):
        subplotaxis.ticker = ticker

    for subplotgrid in p.select({'type': Grid, 'ticker': {'type': DatetimeTicker}}):
        subplotgrid.ticker = ticker

    # subplotgrid = p.select({'type': Grid, 'ticker': {'type': FixedTicker}})
    # for subplotgrid in p.select({'type': Grid, 'ticker': {'type': FixedTicker}}):
        subplotgrid.grid_line_color = 'grey'
        subplotgrid.grid_line_alpha = 1
        subplotgrid.grid_line_dash = 'solid'

        subplotgrid.minor_grid_line_color = 'grey'
        subplotgrid.minor_grid_line_alpha = 0.5
        subplotgrid.minor_grid_line_dash = [5, 5]

    return p

# https://stackoverflow.com/questions/19353576/curved-text-rendering-in-matplotlib
from matplotlib import text as mtext
import math

class CurvedText(mtext.Text):
    """
    A text object that follows an arbitrary curve.

    (c) Thomas Kühn, https://stackoverflow.com/a/44521963/10310883
    """
    def __init__(self, x, y, text, axes, **kwargs):
        super(CurvedText, self).__init__(x[0],y[0],' ', axes, **kwargs)

        axes.add_artist(self)

        ##saving the curve:
        self.__x = x
        self.__y = y
        self.__zorder = self.get_zorder()

        ##creating the text objects
        self.__Characters = []
        for c in text:
            if c == ' ':
                ##make this an invisible 'a':
                t = mtext.Text(0,0,'a')
                t.set_alpha(0.0)
            else:
                t = mtext.Text(0,0,c, **kwargs)

            #resetting unnecessary arguments
            t.set_ha('center')
            t.set_rotation(0)
            t.set_zorder(self.__zorder +1)

            self.__Characters.append((c,t))
            axes.add_artist(t)


    ##overloading some member functions, to assure correct functionality
    ##on update
    def set_zorder(self, zorder):
        super(CurvedText, self).set_zorder(zorder)
        self.__zorder = self.get_zorder()
        for c,t in self.__Characters:
            t.set_zorder(self.__zorder+1)

    def draw(self, renderer, *args, **kwargs):
        """
        Overload of the Text.draw() function. Do not do
        do any drawing, but update the positions and rotation
        angles of self.__Characters.
        """
        self.update_positions(renderer)

    def update_positions(self,renderer):
        """
        Update positions and rotations of the individual text elements.
        """

        #preparations

        ##determining the aspect ratio:
        ##from https://stackoverflow.com/a/42014041/2454357

        ##data limits
        xlim = self.axes.get_xlim()
        ylim = self.axes.get_ylim()
        ## Axis size on figure
        figW, figH = self.axes.get_figure().get_size_inches()
        ## Ratio of display units
        _, _, w, h = self.axes.get_position().bounds
        ##final aspect ratio
        aspect = ((figW * w)/(figH * h))*(ylim[1]-ylim[0])/(xlim[1]-xlim[0])

        #points of the curve in figure coordinates:
        x_fig,y_fig = (
            np.array(l) for l in zip(*self.axes.transData.transform([
            (i,j) for i,j in zip(self.__x,self.__y)
            ]))
        )

        #point distances in figure coordinates
        x_fig_dist = (x_fig[1:]-x_fig[:-1])
        y_fig_dist = (y_fig[1:]-y_fig[:-1])
        r_fig_dist = np.sqrt(x_fig_dist**2+y_fig_dist**2)

        #arc length in figure coordinates
        l_fig = np.insert(np.cumsum(r_fig_dist),0,0)

        #angles in figure coordinates
        rads = np.arctan2((y_fig[1:] - y_fig[:-1]),(x_fig[1:] - x_fig[:-1]))
        degs = np.rad2deg(rads)


        rel_pos = 10
        for c,t in self.__Characters:
            #finding the width of c:
            t.set_rotation(0)
            t.set_va('center')
            bbox1  = t.get_window_extent(renderer=renderer)
            w = bbox1.width
            h = bbox1.height

            #ignore all letters that don't fit:
            if rel_pos+w/2 > l_fig[-1]:
                t.set_alpha(0.0)
                rel_pos += w
                continue

            elif c != ' ':
                t.set_alpha(1.0)

            #finding the two data points between which the horizontal
            #center point of the character will be situated
            #left and right indices:
            il = np.where(rel_pos+w/2 >= l_fig)[0][-1]
            ir = np.where(rel_pos+w/2 <= l_fig)[0][0]

            #if we exactly hit a data point:
            if ir == il:
                ir += 1

            #how much of the letter width was needed to find il:
            used = l_fig[il]-rel_pos
            rel_pos = l_fig[il]

            #relative distance between il and ir where the center
            #of the character will be
            fraction = (w/2-used)/r_fig_dist[il]

            ##setting the character position in data coordinates:
            ##interpolate between the two points:
            x = self.__x[il]+fraction*(self.__x[ir]-self.__x[il])
            y = self.__y[il]+fraction*(self.__y[ir]-self.__y[il])

            #getting the offset when setting correct vertical alignment
            #in data coordinates
            t.set_va(self.get_va())
            bbox2  = t.get_window_extent(renderer=renderer)

            bbox1d = self.axes.transData.inverted().transform(bbox1)
            bbox2d = self.axes.transData.inverted().transform(bbox2)
            dr = np.array(bbox2d[0]-bbox1d[0])

            #the rotation/stretch matrix
            rad = rads[il]
            rot_mat = np.array([
                [math.cos(rad), math.sin(rad)*aspect],
                [-math.sin(rad)/aspect, math.cos(rad)]
            ])

            ##computing the offset vector of the rotated character
            drp = np.dot(dr,rot_mat)

            #setting final position and rotation:
            t.set_position(np.array([x,y])+drp)
            t.set_rotation(degs[il])

            t.set_va('center')
            t.set_ha('center')

            #updating rel_pos to right edge of character
            rel_pos += w-used
