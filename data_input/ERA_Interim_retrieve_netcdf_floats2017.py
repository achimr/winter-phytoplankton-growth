from ecmwfapi import ECMWFDataServer

server = ECMWFDataServer()

#lonmin,lonmax,latmin,latmax=
#360-62.972853,360-60.1413623333333,68.972691,71.5483231666667

server.retrieve({
    'format'    : "netcdf",
    'stream'    : "oper",
    'levtype'   : "sfc",
    'param'     : 'tp/tcc/lcc/mcc/hcc/ssr/str/slhf/sshf/parcs',
    # total precip, cloud fractions, surface heat fluxes
    #for parameter names, see also http://apps.ecmwf.int/codes/grib/param-db?&filter=grib1&table=128
    # attention: par (but not parcs) has issues, see https://confluence.ecmwf.int/pages/viewpage.action?pageId=59018262
    'dataset'   : "interim",
    'step'      : "12",
    'grid'      : "0.75/0.75",
    'time'      : "00/12",
    'date'      : "2017-07-15/to/2019-07-31",
    'area'      : "75/-71/67.4/-60", # latmax, lonmin, latmin, lonmax (North West South East)
    'type'      : "fc", # for forecast; an: reanalysis
    'class'     : "ei", # ERA-Interim
    'target'    : "ERA-interim.nc"
})
