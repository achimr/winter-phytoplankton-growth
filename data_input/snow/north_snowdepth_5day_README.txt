Northern Hemisphere Snow depth files from SMMR (1978-1987189) and SSMI (1987190-2008)

Data processed with a five day history.  This allows some determination of variability in the snowcover due to weather events or melt events, which gets flagged in the dataset.

Files are stored in directories per winter season (WSYYYY_YYYYdata), starting 1 October and ending 30 September the following year.
Files are named ssmi_n_snowdepth_5day_YYYYDDD.img, where YYYY is the year and DDD is the day-of-year.

Int arrays, 304 x 448, Little-endian (macs and unix will need to byteswap)

Values are:

0-100 snow depth in cm
110	missing
120	land
130	open water
140	multi-year ice (no snow calculations done)
150	variability flag (significant changes in the snow depth over the period covered) as a result of weather effects and short-term melt events.
160	summer melt (5 consecutive variable days after April 1; will be flagged melt until 1 October)

Contacts:
Thorsten Markus, Thorsten.Markus@nasa.gov, 301-614-5882
Don Cavaleiri, Donald.Cavaleiri@nasa.gov, 301-614-5901
Alvaro Ivanoff, Alvaro.Ivanoff@nasa.gov, 301-614-5886

readme last updated 9/17/2008