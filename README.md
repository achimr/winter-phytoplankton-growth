[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.4007206.svg)](https://doi.org/10.5281/zenodo.4007206)

## Welcome!

This is the source repository containing code, data and other materials necessary to replicate the analysis of our study "Arctic mid-winter phytoplankton growth revealed by autonomous profilers".

The following Jupyter notebooks contain the core of the analysis and they are dependent on each other, in this order:

- `A1_treat_data.ipynb`
- `A2_viz_ts.ipynb`
- `A3_light.ipynb`
- `A4_biomass.ipynb`
- `A5_celldivision.ipynb`
- `A6_growth_rates.ipynb`
- `A7_export_data.ipynb`
- `B1_ancillary_data.ipynb`
- `B2_map.ipynb`

`imports.py` are imports common to all notebooks. To run them, you will have to install Jupyter and a number of other python libraries, preferably with the package manager of your choice. You can find the environment specifications for conda in `binder/environment.yml`.

## Interactive figures

Supplementary Figs. S3, S4, and S13-15 contain a lot of vertical profiles of various quantities. Their interactive `html` versions mentioned in the Material and Methods section can be found in `./nb_fig/profiles_*.html`.

## Directory structure:

- `./binder`: Setup files for conda environment, `repo2docker`, [mybinder.org](https://mybinder.org), ...
- `./data_input`: Input data (floats, AMSR-2 ice concentration, ancillary atmospheric data, ...)
- `./data_to_archive`: Output data that are separately archived in a public repository (but not tracked with git)
- `./misc_fig`: Figures not created in this repository
- `./nb_fig`: Figures created by notebooks
- `./nb_html`: notebooks rendered to html, for convenience
- `./nb_tmp_data`: Data files created and re-used by notebooks (but not tracked with git)
- `./nb_tmp_fig`: Temporary figures created by notebooks (but not tracked with git)
- `./paper`: Manuscript, notably `paper.md` and `supmat.md`
- `./styles`: Matplotlib style sheet
- `./utils`: Some custom util functions. Local copy of the relevant parts of [poplarShift/python-data-science-utils](https://github.com/poplarShift/python-data-science-utils)

Run `./compile_paper.sh` to convert the paper and the supplementary material to docx formats, and `./compile_notebooks` to clear notebooks from outputs and render them to html.

## Related documents and links

<!-- - Randelhoff, A. et al., Arctic mid-winter phytoplankton growth revealed by autonomous profilers -->
- `doi:10.5281/zenodo.3944836`
- `doi:10.5281/zenodo.3945047`
