#!/bin/bash

for file in `ls [A,B,C]*.ipynb`
do
  fname=`echo $file | cut -d'.' -f 1`
  # clean output
  jupyter nbconvert --ClearOutputPreprocessor.enabled=True --to notebook --output=${fname} $fname
  # render to html
  jupyter nbconvert --to html --output=nb_html/$fname $fname.ipynb
done
