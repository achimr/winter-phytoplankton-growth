---
title: "Arctic mid-winter phytoplankton growth revealed by autonomous profilers"

---

**Authors**: Achim Randelhoff¹²*, Léo Lacour¹², Claudie Marec¹³, Edouard Leymarie⁴, José Lagunas¹²**, Xiaogang Xing⁵, Gérald Darnis¹², Christophe Penkerc'h⁴, Makoto Sampei⁶, Louis Fortier¹², Fabrizio D'Ortenzio⁴, Hervé Claustre⁴, Marcel Babin¹²

**Affiliations**:

¹ Takuvik Joint International Laboratory, Université Laval (QC, Canada) and CNRS (France)

² Département de biologie, Université Laval and Québec-Océan (QC, Canada)

³ Institut Universitaire Européen de la Mer, 29280 Plouzané, France

⁴ Sorbonne Université, CNRS, Laboratoire d'Océanographie de Villefranche (LOV), 06230 Villefranche-sur-Mer, France

⁵ State Key Laboratory of Satellite Ocean Environment Dynamics, Second Institute of Oceanography, Ministry of Natural Resources, Hangzhou, China

⁶ Faculty of Fisheries Sciences, Hokkaido University, Hakodate 041-8611, Japan

\*\* now at: Defense Research and Development Canada - Atlantic, 9 Grove St, B3A 3C5 Dartmouth, Nova Scotia, Canada

\* Correspondence to: achim.randelhoff@takuvik.ulaval.ca

**Abstract**: It is widely believed that during winter and spring, Arctic marine phytoplankton cannot grow until sea ice and snow cover start melting and transmit sufficient irradiance, but there is little observational evidence for that paradigm. To explore the life of phytoplankton during and after the polar night, we used robotic ice-avoiding profiling floats to measure ocean optics and phytoplankton characteristics continuously through two annual cycles in Baffin Bay, an Arctic sea that is covered by ice for seven months a year. We demonstrate that net phytoplankton growth occurred even under 100% ice cover as early as February, and that it at least partly resulted from photosynthesis. This highlights the adaptation of Arctic phytoplankton to extreme low-light conditions, which may be key to their survival before seeding the spring bloom.

**One Sentence Summary**: In spite of polar night and thick sea ice, algae can grow in the wintertime Arctic Ocean.

**Main Text**:

# Introduction

Arctic waters are subject to long periods of darkness. The polar night and an extensive, reflective, and relatively opaque ice and snow cover impose extreme conditions on photosynthetic algae during winter and spring [@sakshaug2004primary].
To the extent that under-ice blooms have been observed at all, they were linked to sufficient light penetration through leads or melt ponds in the sea ice [@fortier2002climatic; @mundy2014role; @arrigo2014phytoplankton; @assmy2017leads].
Consequently, the light field under snow-covered ice is assumed not to be sufficient to produce high phytoplankton biomass, and increases in the frequency of such under-ice blooms during the last decades have been postulated [@wassmann2011arctic; @horvat2017frequency] based on a thinning and hence increasingly transparent ice cover [@comiso2012large].
Yet, new studies suggest that marine algae may be able to grow under extremely low irradiances [@hancke2018extreme].

<!-- research rationale and methods -->
Whether such phytoplankton winter growth exists and how it shapes Arctic phytoplankton ecology is so far an open question due to observational challenges.
Starting in 2015, we have been deploying autonomous biogeochemical (BGC) Argo floats [@mayot2018assessing] in Baffin Bay, an Arctic sea where the sun is continuously below the horizon for more than two months each winter and a thick sea ice cover lasts well into July [@tang2004circulation].
Equipped with an ice avoidance system that allows continuing sampling through winter, these specially adapted floats measure vertical profiles of hydrographic and bio-optical properties.
In summer 2018, we retrieved the first time series of annual phytoplankton dynamics observed by such BGC-Argo floats in the Arctic Ocean.

# Results and Discussion

<!-- results -->
Four floats, covering Baffin Bay from summer 2017 through summer 2019 (@Fig:map_hydrography), measured pronounced annual cycles of phytoplankton biomass as evidenced by two proxies:
Particle backscattering at 700 nm (bbp) and chlorophyll _a_ fluorescence (chl-a) ([@Fig:growth]C, E), varying over one (bbp) to two (chl-a) orders of magnitude in the surface layer, defined as the deeper one of either mixed layer or the layer with photosynthetically available radiation (PAR) larger than 0.4 mol photons m$^{-2}$ d$^{-1}$, a value commonly used to bound the zone of net phytoplankton growth [@letelier2004light; @boss2010situ].
Phytoplankton net specific growth rates _r_ (d$^{-1}$, [@Fig:growth]G, H) were calculated from surface-layer chl-a and bbp.
Briefly, as entrainment of low-biomass water into the surface layer will dilute the phytoplankton standing stock [@boss2010situ], growth rates were based on vertically integrated biomass during the winter when the mixed layer deepened and on vertically averaged values otherwise.
Growth rates averaged a weak but significant 0.011 d$^{-1}$ for chl-a in February and March (p=0.007, N=11 estimates).
Growth rates based on bbp turned positive one to two months later, averaging 0.016 d$^{-1}$ in April (p=0.007, N=5 estimates), likely because low biomass during winter rendered much of the surface layer averaged backscattering indistinguishable from the background signal.
The rapid increase in mixed-layer average chl-a and bbp in early May coincided with when the surface mixed layer stopped deepening ([@Fig:growth]A, E).
The ERA-Interim reanalysis [@dee2011era] indicated that at the same time, the net ocean surface heat flux switched from upward to downward ([@Fig:environment]A), effectively stopping convective dilution due to brine rejection during sea-ice growth and permitting mean biomass to increase.

<!-- light results -->
Our results show that Arctic phytoplankton can grow at extremely low light levels.
Indeed, snow and sea ice strongly attenuated the irradiance reaching the upper ocean.
From December until early February, underwater PAR from 12 m downward (the shallowest depth sampled by the floats under the ice) did not exceed the PAR sensor's noise level (approximately 0.25 µmol photons m$^{-2}$ s$^{-1}$) even at local noon.
In early February, noon sun elevation at 71 °N is 5° and a geometric model of clear-sky radiation would have predicted irradiances an order of magnitude higher than we observed due to ice, snow, and clouds.

<!-- cell division calculation: autotrophic growth possible  -->
To determine whether photosynthesis could explain the observed net specific phytoplankton growth rates despite extremely low light levels, we modelled cell division rates µ using _in situ_ irradiance profiles and photosynthetic parameters.
For the latter, we measured maximum growth rate and the light saturation irradiance during two April through July ice camp expeditions in Baffin Bay in 2015 and 2016 and supplemented these with earlier observations in Baffin Bay in late summer [@bouman2018photosynthesisirradiance].
Cell division rates started to increase above zero in February with the return of the sun ([@Fig:growth]G, H and [@Fig:environment]C), indicating that observed winter net growth rates of chl-a are realistic and may reflect phototrophy at extremely low light levels.
This is in line with an intact photosystem throughout the polar night [@kvernvik2018fast] and a theoretical minimum irradiance of approximately 10 nmol photons m$^{-2}$ s$^{-1}$ for phytoplankton growth [@raven2000put], more than ten times below our PAR sensor's noise threshold.
This theoretical minimum neglects respiration, which previous research has indeed shown to be extremely low in polar diatoms during extended darkness [@lacour2019decoupling; @morin2019response].
Standing stocks of algae growing in the ice are usually $\ll1$ mg chl-a m$^{-2}$ until April [@leu2015arctic], with sloughing not occurring before the end of the sympagic bloom when the ice matrix warms up in late spring [@cota1989physical]. Ice algae therefore likely made at most a minor contribution to the low-light growth we observed.
For some plankton, such winter growth may also be supported by heterotrophy [@lovejoy2007distribution; @joli2017seasonal], an exciting phenomenon that should be further investigated in future Arctic expeditions.

<!-- how could we observe this little growth -->
The match between the patterns of modelled cell division rates and that of observed net growth rates _r_ from February through May also suggests negligible losses to herbivorous grazing.
Mixed layer deepening ([@Fig:growth]A) along with biomass losses during fall and early winter reduced mean chlorophyll _a_ concentration to as little as 0.02 mg chl-a m$^{-3}$.
Such levels may be below the threshold prey concentrations at which copepods stop feeding [@frost1993modelling], drastically reducing herbivorous grazing both by copepods and by microzooplankton [@strom2001phytoplankton].
Lower water temperatures during winter presumably also inhibited zooplankton growth more than phytoplankton growth [@rose2007does].
Reduced winter grazing of herbivorous zooplankton was likely exacerbated due to diapause of _Calanus_ spp., a key complex of species in Arctic ecosystems [@falk-petersen2009lipids], which lead to one order of magnitude lower 0-60 m integrated mesozooplankton biomass in winter as compared to summer as shown by vertical net hauls in the Beaufort Sea [@darnis2014temperature], similar to Baffin Bay in term of plankton dynamics.
Once returned from diapause, copepods can indeed exert significant grazing pressure:
According to our measurements from 24 May to 7 June at the 2016 Green Edge ice camp in Baffin Bay, feeding rates (1.9 mg C m$^{-2}$ d$^{-1}$, N=4) represented half of net primary production, which at that point was still at pre-bloom levels.

# Conclusions

In summary, we were able to observe faint but significant phytoplankton growth during winter under the ice likely because of photosynthesis at extremely low light levels (in addition to possibly heterotrophy) and small losses to respiration and herbivorous grazing.
Such growth may alleviate cell mortality during the long winter darkness and help to seed the spring bloom.
The conspicuous summer biomass maximum was in that sense the culmination of a long period of winter phytoplankton growth rather than a singular event.
Net specific phytoplankton accumulation rates _r_ in fact peaked in April and May when ice concentration was 100% and showed no sign of retreating for another two months ([@Fig:environment]B).
Like at lower latitudes [@behrenfeld2014resurrecting; @mignot2018floats], we thus find that Arctic marine phytoplankton biomass cycles may be determined by seasonal changes in cell division rates and a time-lagged grazer response, caused by a decoupling of growth from grazing during winter.

<!-- Conclusion -->
It is already known that feeding and migration patterns of zooplankton, benthos, and fish communities do not necessarily cease during the polar night [@berge2015unexpected], perhaps cued by barely perceptible variations in the light field [@cohen2015ambient]. We provide evidence that winter is not a dormant period even at the very base of the food web -- likely the result of phytoplankton's sophisticated adaptation to its environment, as extreme as it may appear.

# Materials and Methods

## ProIce float description

The "ProIce" Argo floats, developed by Takuvik and the _Laboratoire d’Océanographie de Villefranche_ (LOV) and especially adapted to ice-covered regions, were equipped with a suite of oceanographic and bio-optical sensors.
Ten such floats [were deployed in Baffin Bay in July 2017 and 2018](http://www.obs-vlfr.fr/proof/php/GREENEDGE/greenedge_autonomous.php) as part of the Takuvik contribution to the BGC-Argo program, see Table S1 [@biogeochemical-argoplanninggroup2016scientific]. Only three of the seven floats deployed in 2017 surfaced in the following summer; one had stopped sampling in April 2018. In July 2018, two more floats were deployed. Both survived the winter along with one more from the previous season, but one of the newly deployed floats re-surfaced south of Davis Strait and has been neglected in our analysis. In summary, two floats (012b and 017b) sampled the winter 2017-18, one float sampled the winter 2018-19 (020b), and one float sampled both winters 2017-19 (016b).

### Sensor payload

ProIce floats are equipped with a CTD (conductivity-temperature-depth) Seabird41 unit, an Aanderaa 4330 oxygen optode, a Wetlabs REM-A (combining OCR504 radiometer (380 nm, 412 nm, 490 nm and PAR, i.e. Photosynthetically Available Radiation) and an ECOTriplet  for the observation of chlorophyll _a_ fluorescence, colored dissolved organic matter, and particle backscattering at 700 nm (bbp), as well as a Satlantic SUNA V2 sensor for nitrate measurements.

### Design and mission management

The Argo float model used in this study is the so-called ProIce float, manufactured by the French company NKE. It is based on the Provor CTS5 developed under a collaboration between NKE and LOV [@leymarie2018proval]. This float has the same mechanical characteristics as the previous version (CTS4) and can carry the entire suite of biogeochemical sensors. It does not require pre-ballasting as a function of mean seawater density, a substantial advantage in Polar Regions where low-density surface layers are present [@riser2016fifteen]. Mission parameters are highly flexible and can be changed both in real time by Iridium communication and in advance by means of a script file. The latter has been used in this study to modify the float sampling schedule under ice without any satellite communication.

Adaptation of the CTS5 float to Arctic conditions was carried out in a close collaboration between LOV and Takuvik Laboratories in the framework of the NAOS project. Takuvik lead field trials in a frozen lake close to Québec city and at the Green Edge ice camp in Qikiqtarjuaq (Nunavut) in order to validate the reliability of the float and sensors in polar conditions. Lagrangian trajectories were simulated to choose the best dropping zones, combined with observations from historical climatology and ice charts.

### Ice avoidance

Argo floats must surface for geo-localization, data transmission and command reception using satellite networks [@riser2016fifteen]. Sea ice, which can damage floats, must be avoided  to make the floats operational in the Arctic Ocean [@fennel2017taking]. If sea ice is present at the surface, Argo floats need to postpone surfacing. It then has to perform several consecutive profiles storing data during wintertime.

ProIce floats use a combination of three technologies to prevent surfacing in the presence of sea-ice: an upward looking Teledyne PSA-196 altimeter, an Ice Sensing Algorithm - ISA [@klatt2007profiling] based on local temperature, and a simple date criterion.

The Altimeter was used to compare the depth to the distance to the surface in order to detect ice cover. Due to the uncertainties on both the depth and the distance, a threshold of 5m was used which correspond mainly to the presence of an iceberg. This triggering  never occurred for floats deployed in the present study.
We used a substantial database of temperature and salinity profiles  and ice cover observations in Baffin Bay in order to locally adapt ISA’s parameters. The floats were hence programmed to abort surfacing whenever the median temperature between 30 and 10 dbar was below -1.3°C. As Arctic surface waters exhibit high variation of salinity, we assume that these thresholds are highly local and should be adapted before being applied to other Arctic seas.

Finally, we prevented surfacing between November and July when the probability of sea-ice was very high. This was assessed based on a survey of climatological sea-ice cover data. An optical device, specifically designed by Takuvik to detect sea-ice based on laser polarimetry [@lagunas2018seaice], was tested with promising results but not used in operation.

Since floats did not sample shallower than 10 m when ice was present, some phytoplankton biomass was likely missed in July which explains the fact that the spring/ice-edge bloom is not more pronounced in Fig. 2E of the main text.

### Sampling schedule

Briefly, the floats acquired daily profiles in the ice-free summer period, every fourth week during winter, and every 10th day in spring (Table S2). Profiles were terminated at 15:00 UTC each, i. e. approximately local noon.

In mid-November, the floats entered a winter cycle of one profile every 14 (float 020b) or 28 days (other floats) ending in late June the following year. During this period, the up-casts were limited to 10 dbar to prevent any collisions with sea-ice on the surface. Starting in July, profiles were collected every 10th day; the floats surfaced after each profile if not prevented by the ice detection system. Daily profiling was programmed from August 1 until November 15.

## Data processing methods

### Float sensor validation

RemA sensors (OCR504 and ECO triplet) data are collected in dark conditions before deployment to evaluate their offset drift since factory calibration. A night profile is programmed as an in-situ check of this offset down to 1000m to meet relatively large temperature dynamics, helping to see the response of OCR to temperature.

Analysis onboard for cross validation:
A 0-1000m CTD rosette cast is systematically performed after the deployments and water sampled to provide an evaluation and characterization of the calibration errors of the biogeochemical sensors (Mignot et al., 2019, Taillandier et al., 2017):
-10 levels in the 100m upper layer for shipboard chl-a measurements, as well as for High Performance Liquid Chromatography (HPLC) for chlorophyll concentration performed by CNRS/ SAPIGH unit in France for the validation of the chl-a fluorometer.
- 10 levels in the water column for FDOM analysis onboard by means of an Ultrapath unit for validation of the CDOM fluorometer.

Chlorophyll _a_ concentration was determined with a WETLabs ECO chlorophyll _a_ fluorometer. Raw chl-a fluorescence was smoothed with a 5-point median filter in order to remove noise and spikes, and then dark-corrected based on deep values [@xing2018improved]. In the third step, non-photochemical quenching was corrected [@xing2018improved], and finally, the slope (F-factor) was determined based on the comparison between the first float profile and on-board fluorometry. Corrected chlorophyll _a_ fluorescence was calculated as F · (FChlaraw – FChlaDark).

Each profile of particulate backscattering data (bbp) was smoothed using a 5-point median filter.

### Mixed layer depth

Mixed layer depth was calculated as the shallowest depth where density ($\sigma_{\theta}$) exceeded the surface density by at least 0.1 kg m$^{-3}$ [@peralta-ferriz2015seasonal]. Surface density is defined as the 0-15 m average value to be consistent between summer and winter, when the shallowest sampling depth was 12 m. The density criterion of 0.1 kg m$^{-3}$ is higher than those commonly utilized at lower latitudes [@montegut2004mixed] due to strong Arctic stratification.

### Light model

A radiative transfer model [@morel1991light] was used to complement float measurements in order to compute diurnally integrated PAR profiles along the float trajectory following @lacour2017unexpected. Briefly, the model provided theoretical clear-sky PAR values just below the sea surface with a time increment of 1/60 of the day length. From these values, instantaneous PAR (iPAR) profiles were created using a diffuse attenuation coefficient derived from the chl-a profile measured by the float [@morel1988optical]. By comparing the float iPAR profile with the one modelled for the same time and location, the light attenuation due to the cloud cover and the sea ice cover was estimated. The mean, over the vertical profile, of the ratio of float iPAR to modelled iPAR, weighted by absolute values of float iPAR, gave a correction factor (Fig. S1) that was subsequently applied to all the modelled profiles of the day. The underlying assumption was that both the average cloud and sea ice cover estimated during the float ascent were representative of the day. The vertical profile of daily integrated PAR was then computed by time-integrating all these corrected iPAR profiles over the day length.

#### PAR noise level during winter

The instantaneous PAR noise level was found to be 0.25 µmol m$^{-2}$ s$^{-1}$ [for a more detailed treatment, see @xing2018laboratory]. During most of winter, profiles of iPAR at local noon exhibited values above this threshold, hence permitting the fit of model-derived clear-sky daily PAR to the instantaneous values as described above. Some profiles of iPAR (in December through early February, but also late April, when a significant drop in under-ice PAR occurred), however, do not contain any values above the noise threshold at 12 m depth (the shallowest observation depth during winter) at local noon. We next calculated the maximum possible daily PAR that could have been available given the fact that the sensors sampled only noise. To this end, we constructed an iPAR profile for noon, which we then modulated according to the sun angle throughout the day and integrated over one diurnal cycle.
The upper limit of iPAR was constructed setting PAR to the noise threshold at 12 m depth and supposing a clear-water extinction coefficient of 0.07 m$^{-1}$ [@sakshaug2004primary] in order to extrapolate the artificial PAR profile to the ocean surface and across the rest of the water column.
Values at each depth were diurnally modulated using the sine of the solar elevation angle during daytime, but an exponential decrease during twilight and night [@ekstrom2002blue; @ekstrom2007error].
This is rapid enough to be practically zero (hence consistent with the sine weight) if the sun appears over the horizon at any time of the day, but allows for translation between iPAR at noon and daily PAR even during the polar night.
The winter cell division rates based on these maximum artificial light profiles are clearly marked in Fig. 2G of the main text.

### Specific phytoplankton growth rates

Net specific phytoplankton growth rates (d$^{-1}$) were calculated based on surface layer average and integrated values as follows, adapting the methodology developed by [@boss2010situ]. For each profile, the surface layer `sfc` was defined, for each profile, as the larger of z0.4 (the 0.4 mol m$^{-2}$ d$^{-1}$ isolume [@letelier2004light]) and the mixed layer depth, which in practice meant the mixed layer depth throughout winter and the isolume depth through summer (Fig. S2). chl-a and bbp were then both integrated ($\int_\mathrm{sfc} \mathrm{d}z \cdots$) and averaged ($\langle\cdots\rangle_\mathrm{sfc.}$) over the surface layer.

An isolume depth, i.e. the depth of a specific daily irradiance level, is commonly employed [@letelier2004light; @lacour2017unexpected] to bound the vertical extent of net phytoplankton growth. We chose $z_{0.4}$ as the value previously found both to vertically constrain the extent of phytoplankton net growth in subtropical oceans [@letelier2004light] and to terminate the Arctic fall bloom in Baffin Bay. We hence assume that this value takes into account grazing rates typical of summer. Because during winter, the mixed layer depth was always deeper than any reasonable choice of an isolume depth, we do not need to determine a corresponding isolume criterion for winter.

Biomass changes were primarily reflected by changes in mean concentration, with the exception of possible convective dilution during winter, when the mixed layer deepened. To calculate the 'net specific growth rate', we stipulate, still following [@boss2010situ], that during mixed layer deepening in winter, the total phytoplankton biomass is represented by the integrated chl-a or bbp to account for dilution by entrainment of low-biomass water from below the mixed layer. As chl-a and bbp observed in Arctic winter are extremely low (Figs. S3 and S4), biomass below the mixed layer is frequently not significantly less than in the mixed layer, and hence increases in the integrated biomass only reflect the deeper integration limit, not a real signal. Accordingly changes in the mean biomass should be used to infer growth rates then as well. When the mixed layer is shoaling, or the zone of net phytoplankton growth (here stipulated as being bounded by the 0.4 mol m$^{-2}$ d$^{-1}$ isolume) extends deeper than the mixed layer, detrainment (loss) of higher-biomass water from the mixed layer is happening, and so biomass changes are reflected by changes in mean concentration too.

In summary, the net specific growth rate _r_ was calculated from the integrals

$$r=\frac{\Delta \int \mathrm{d}z \cdots}{\Delta t \cdot \int \mathrm{d}z \cdots}$$

if and only if all three of the following conditions are fulfilled:
1. if the mixed layer was deepening ($\Delta \mathrm{MLD}>0$, where MLD stands for mixed layer depth), hence if entrainment can have taken place at all;
2. the mixed layer was deeper than the 0.4 mmol photons m$^{-2}$ d$^{-1}$ isolume (MLD>z0.4); and
3. there was a statistically significant (p<0.05) difference between mixed-layer average biomass (bbp or chl-a) and the biomass averaged from the mixed layer base to 15 m below the mixed layer (see computer code).

In all other cases, _r_ was calculated using the difference of the means:

$$r=\frac{\Delta \langle \cdots \rangle}{\Delta t \cdot \langle \cdots \rangle}.$$

To recapitulate, criteria 1 and 2 are following a published methodology [@boss2010situ], while we have added the restrictive criterion 3 to account for weak winter biomasses that may render invalid the original reasoning. In order to account for values that may be at or below the noise levels, we further discarded any growth rate where the surface layer mean biomasses in both involved profiles was below the environmental background (see below for the calculation of environmental background variability level).

The entire growth rate calculation procedure is illustrated in Figures
S5 to S12.
Biomasses had to be averaged in bins before calculating the growth rates (see below for a more detailed explanation). Taking as an example a bin width of 14 days, starting on 2017-7-20, we have a total of 180 estimates of the specific change of phytoplankton biomass (14 time periods x 4 Floats x 2 biomass proxies, i.e. chl-a fluorescence and bbp, but not every float was reporting during every bin). Of these, 12 were rejected because the associated biomasses were not significantly different from the environmental background variability. For the remaining 168 values, the net growth rate _r_ was calculated based on the surface layer integrals 33 times, or 19 % of the time, while the rest (81 %) were calculated based on the differences in mean values.

This methodology does not take into account that active mixing may not extend to the base of the mixed layer at all times [@brainerd1995surface], but most temperature and salinity profiles during winter exhibited steep density gradients at the mixed layer base (Figures S13 to S15), indicating strong mixing throughout winter.

As common parameterizations for biomass are linear in bbp and chl-a, respectively [@behrenfeld2006beam], the specific biomass growth rates do not depend on the numeric values of the conversion factors between chl-a, bbp, and biomass (in g C m$^{-3}$).

Further exploring the robustness of growth and decay patterns, we also wrapped the entire time series into a single annual cycle, smoothing it using a Generalized Additive Model with 12 splines, and calculating the growth rates as described above The results can be seen in Fig. S16 and Fig. 2H of the main text.

### Sensitivity of specific net growth rates to biomass binning

The above algorithm calculates growth rates from successive differences of either surface layer integrated or surface layer averaged chl-a or bbp. Especially in summer, time series are very noisy, necessitating aggregating those surface layer mean or integrated biomasses further in e.g. 7-daily or 28-daily bins. To address the sensitivity in the growth rate calculation to the exact choice of how we binned each time series, we varied the bin edges to conduct a parameter sweep. Specifically, bin width was in turn set to 7, 14, 21, or 28 days, and for each bin width, the bin edges were successively positioned in 1-day intervals. (That means for example for a bin width of 7 days, bin edges were placed on [2017-7-1, 2017-7-8, 2017-7-15, …] for one iteration, [2017-7-2, 2017-7-9, 2017-7-16, …] for another, and so on.) See Fig. S17 for an illustration.

As the focus is on the winter period, when each float sampled at 14- or 28-day intervals, we also investigated the difference in growth rates based on biomasses calculated from biomass averaged in 14- and 28-day bins. (In practice, this means calculating each winter growth estimate from either successive profiles or from successive means of each two profiles.) For these a Kruskal-Wallis test confirmed that calculated growth rates  did not significantly vary from one method to another (Fig. S18).

### Sensor noise levels, significance of low chlorophyll _a_ and backscattering measurements, and other noise sources potentially contaminating winter biomass growth rates

Given the low biomasses measured in winter, one has to worry about the statistical significance of any results derived from them. In this section, we show that most noise sources were not strong enough to potentially contaminate the signal and shadow the real growth rate. Three noise sources have to be considered: Noise stemming from the sensor itself, noise stemming from (random or small-scale) environmental variability, and environmental spatial trends (such as a potential steady drift into higher-chlorophyll water masses during winter). We address each of these in turn.

First, let us consider sensor noise. Wetlab’s/Seabird ECO sensors (that measure bbp and chl-a) state a resolution of 1 digital count for both types of sensors of the ones mounted on our floats. This is defined as the standard deviation of a sensor sampling a static target at 60Hz for 60 seconds (i.e. a sample size of n=60).
For chlorophyll _a_, the factory calibration is that 1 count = 0.0073 mg chl-a m$^{-3}$, which after QC using on-board HPLC drops by a factor of approximately two down to 0.0037 mg chl-a m$^{-3}$. (This is the slope factor mentioned in Table S3. This factor is also in line with a global validation of the ECO sensor [@roesler2017recommendations] which recommended applying a factor of two to reduce all measurements using this sensor.) A float samples the ECO sensors at every 10 cm of depth, and therefore the typical “surface layer averaged chlorophyll _a_” value is comprised of n=300 samples for a surface layer of e.g. 30 meters depth. The standard error $\sigma\sim\mathrm{stdev}/\sqrt{n}$, that is the expected deviation of the sample mean from the “true” population mean, is therefore much lower than one count. The lowest mixed layer chlorophyll _a_ values we measured, on the other hand, were an order of magnitude larger, and therefore very much significant. For bbp, 1 count corresponds to around 1.23$\cdot 10^{-5}$ m$^{-1}$. The smoothed time series of averaged bbp displayed in new Fig. 2H climbs from 1.9$\cdot 10^{-4}$ to around 2.5$\cdot 10^{-4}$ from early March to mid April, again a much larger increase than sensor noise.

To estimate the chlorophyll equivalent of such an increase in 700 nm backscattering, one can use literature values of the chlorophyll-to-backscattering ratio. Concretely, at 700 nm, backscattering can be decomposed into $b_{bp,700} = \tilde{b}_{bp, 700} \times b_{p,700}^\ast \times chl-a$, where $\tilde{b}_{bp, 700}$ is the backscattering ratio at 700 nm and $b_{p,700}^\ast$ is the chlorophyll _a_ specific scattering coefficient at 700 nm.
The backscattering ratio at 700 nm is in the range $10^{-4}$ to 3$\cdot10^{-3}$ [@stramski2001modeling] and the chlorophyll _a_ specific scattering coefficient at 700 nm is 0.1-0.3 m$^2$ (mg chl-a)$^{-1}$ [@bricaud1988optical].
This means that 1 chl-a count (0.0037 mg chl-a m$^{-3}$) corresponds to a backscattering increase as little as 3$\cdot10^{-8}$ to 3$\cdot10^{-6}$ m$^{-1}$, demonstrating that the chlorophyll a fluorescence signal is much more sensitive to the phytoplankton concentration than the backscattering sensor is. The environmental (non-phytoplankton) background of the backscattering signal can hence be expected to play a more important role when biomasses are low, as the next paragraph details.

To estimate the environmental background levels and their variability, we calculated the mean chl-a and bbp over 750-800 m depth of every profile (800 m is the maximum depth of Davis Strait and therefore the deepest depth that we can expect to be reasonably well ventilated and not potentially accumulate an excess of detritus), we find for chlorophyll _a_ a standard deviation of 0.0025-0.0035 mg m$^{-3}$ depending on the float, just below one digital count, again much smaller than the lowest average mixed layer values.
For bbp the standard deviation is 1.5—8.7$\cdot 10^{-5}$ m$^{-1}$, which is a more appreciable fraction of the winter increase in bbp, but still relatively small. It also reflects the fact that bbp has a higher non-biological background value than chlorophyll _a_, as is also evidenced by the fact that mixed layer bbp values essentially floor at the bbp baseline in the new Fig.2, panel E (which is not the case for chl-a, which always stays much above its baseline). These 750-800 m averages are 2$\cdot 10^{-4}$ $\pm$ 5$\cdot 10^{-5}$ m$^{-1}$ for bbp and 0.0059 $\pm$ 0.0029 mg chl-a m$^{-3}$ for chl-a (Fig. 2E of the main text). Overall drift of these background values (as determined by an ordinary least squares regression) were mostly statistically insignificant, and the associated drift in values over a year was always less than the standard deviation.

Thirdly, larger-scale changes in the environment have to be considered, such as the floats drifting, during the course of winter, into higher-biomass waters. As Argo floats are never 100% Lagrangian, this may be an explanation for some short-term changes, besides spatial patchiness, but extremely unlikely to affect growth rates over several months: 1) inferred growth rates are consistent between two consecutive years, reducing the chances of a statistical fluke, 2), because water mass properties as sampled by the entirety of our Argo fleet did not systematically change over the course of the year (see Fig. 1D of the main text), and 3) because all floats exhibited increasing biomasses during most of winter even individually.

In summary, it is true that during the dead of winter, environmental variability in the backscattering data renders many of the backscattering-based growth rates unreliable, and these have been excluded from the analysis as described above. However, some remain above this noise level and give positive growth levels, albeit later than chl-a, which is always above noise levels and demonstrates growth as early as February (seen over both years). In other words, for biomasses as low as we have observed, particulate backscattering is not a sensitive enough proxy, and the zero growth rates are likely merely an artefact rather than part of the phenology.

### Photosynthetic parameters

In addition to using literature values for summer Baffin Bay photosynthetic parameters [@bouman2018photosynthesisirradiance], photophysiological experiments were conducted off the fast ice in Baffin Bay in 2015 and 2016 during the Green Edge ice camps [@oziel2019environmental]. Methods are described in [@massicotte2020green], but briefly, we determined $E_k$, the light saturation irradiance for photosynthesis, and $P_\mathrm{b,max}$, the chlorophyll _a_ specific light saturated growth rate, using the models
$P=P_s\cdot(1−e^{−\alpha E})\cdot e^{−\beta E} +P_0$ (when photo-inhibition was apparent)
and
$P=P_s\cdot(1−e^{−\alpha E})+P_0$ (when no photo-inhibition was apparent).
Here $P$ is the rate of photosynthesis and $E$ is the light intensity; the rest are free parameters.
$P_\mathrm{b,max}$ was then calculated by normalizing
$P_m = P_s \cdot \frac{α}{α + β} \cdot \left(\frac{α}{α + β}\right)^\frac{β}{α}$ to chlorophyll _a_ concentrations, and $E_k = \frac{P_m}{α}$.

Values were averaged for winter (November through May), spring (June) and summer (July through October), see Fig. S19.

### Cell division rates

To investigate the relationship between phytoplankton net growth _r_ and cell division rates µ, we modelled light-field modulated variations in phytoplankton cell division rates based on the previously calculated under-water irradiances and photosynthetic parameters. We calculate photosynthesis (in g C (g chl-a)$^{-1}$ d$^{-1}$) as [@lewis1983small]:

$$ \mu_b = \int_\mathrm{24\ hrs} \int \mathrm{d}z P^C_\mathrm{max}  \tanh \left( \frac{PAR}{E_k} \right),$$

and then µ as µb/θ, where θ is the carbon-to-chlorophyll ratio, here assumed to be 30 (g C) (g chl-a)$^{-1}$ [@sakshaug2004primary; @behrenfeld2016revaluating].

The original formula [@lewis1983small] gives an integral from sunrise to sunset, assuming that light is not sufficient for photosynthesis when the sun is below the horizon. We use an integral over an entire cycle of 24 hrs, weighting daily PAR by the sine of the solar elevation angle, but an exponential decrease during twilight and night as described above. During winter, when PAR did not exceed noise values, we used the noise PAR profiles (derived above) to determine the upper limit of cell division rates that could have been possible given the combinations of instantaneous PAR noise floor at noon, shallowest measurement depth, and sun angle.

As a reality check, we compared modelled cell division rates µ with the specific net phytoplankton growth rates _r_, showing good agreement for March through May when averaged over all floats (Fig. S20), but considerable scatter when considering each float on its own (Fig. S21).

### Mesozooplankton

Mesozooplankton stratified net sampling in the Amundsen Gulf during the IPY-CFL 2007-2008 overwintering expedition (Fig. S22) was described by [@darnis2014temperature]. Biomass concentration was calculated from mesozooplankton counts and species-specific length-carbon content relationships. From three stratified nets (0-20 m, 20-40 m, and 40-60 m), biomass was integrated (g C m$^{-2}$) and then divided by the 60-m thickness of the layer, expressing biomass as g C m$^{-3}$). Fig. S23 shows that Baffin Bay and Amundsen Gulf are similar in both abundance and mesozooplankton community structure.

### Grazing experiments

Zooplankton samples for estimation of fecal pellet production were collected from the fast ice once every 2-7 days between 24 May to 7 July 2016 as a part of the Green Edge 2016 field campaign in the Baffin Bay using a 200 μm mesh net hauled vertically from 100 m (before 25 June) and from 30 m (after 25 June) depth to the surface and incubated following @sampei2009attenuation. Fecal pellets samples (100%) were counted and its shape measured under a stereo-microscope (20-40 x magnification). Fecal pellet production rate (mg C m$^{-2}$ d$^{-1}$) was estimated based on fecal pellet volume, gut clearance time of 33 min for Calanus glacialis stage CV and CIV (adult female) in Northern Baffin Bay [@tremblay2006trophic], and a conversion factor from fecal pellet volume to particulate organic carbon contents as 0.048 mg C mm$^{-3}$ [@gonzalez1994possible]. To remove the effects of variation in the volume filtered due to the difference of net towing depth (30 m or 100 m), fecal pellets production rate was standardized for those data as towing distance of 100 m (i.e., the production rate per square meter = Raw data ×100 / towing depth).

## Early May 2018 drop in under-ice PAR

Under-ice surface PAR was almost an order of magnitude lower in the beginning of May 2018 than in the beginning of April the same year. The patterns are consistent for both floats (one float malfunctioned after April 2018), which points to a large-scale cause. Cloudiness is well-known to significantly reduce the Arctic underwater light field [@sakshaug2004primary; @wang2005arctic], but a reanalysis [@dee2011era] did not show increasing cloud optical thickness during that time (Fig. S24). Snow cover determined from satellite passive microwave data [@markus1998snow] showed a peak in snow depth in early May (Fig. S25). In addition, the ERA-Interim reanalysis [@dee2011era] indicated a significant amount of precipitation in April, more than during the previous months. Fresh snow is known to attenuate incoming solar radiation considerably and more so than equally thick layers of sea ice or old or wet snow [@mundy2005variability]. In fact, a snowfall of only 5 cm of fresh snow in early May during the Green Edge 2015 ice camp in Baffin Bay reduced the under-ice irradiance by a factor of four [@verin2019proprietes]. A springtime accumulation of fresh snow is hence the best explanation for the observed drop in under-ice PAR.

## Ancillary data

AMSR2 sea ice concentration data [@spreen2008sea] on a 3.125-km grid were downloaded from [www.iup.uni-bremen.de:8084/amsr2data/asi_daygrid_swath/n3125/](http://www.iup.uni-bremen.de:8084/amsr2data/asi_daygrid_swath/n3125/). Reanalysis fields (total precipitation and ocean-atmosphere heat fluxes) from the ERA-Interim product [@dee2011era] were downloaded through the ECMWF's Python API. NASA's "Snow depth on Arctic sea ice" product [@markus1998snow] was downloaded from [https://neptune.gsfc.nasa.gov/csb/index.php?section=53](https://neptune.gsfc.nasa.gov/csb/index.php?section=53).


**Acknowledgments:** Michel Gosselin's team from Rimouski (Joannie Charette and Aude Boivin-Rioux) contributed chlorophyll _a_ analysis for float sensor quality control. We also thank Flavienne Bruyant, Marie-Hélène Forget, Joannie Ferland, Jade Larivière and Philippe Massicotte for field sampling and data processing of the photosynthetic parameters. Eric Rehm contributed Lagrangian simulations of float trajectories to find deployment locations. **Funding:** The biogeochemical Argo floats used for this study were developed and deployed through the NAOS project (grant #ANR-10-EQPX-40) with help from the Green Edge project. The Canadian Foundation for Innovations (CFI) John R. Evans Leaders funds #30124 covered the acquisition of 9 BGC Bio Argo floats. The European Commission Horizon 2020 grant #727890 to the Integrated Arctic observation system INTAROS covered travel expenses related to the deployment of the floats and coordination efforts. Takuvik’s laser ice detection project was financially supported by the French National Center for Scientific Research (CNRS) through their program for innovation and technology development _Defi Instrumentation aux limites_ (2015). We thank the captain and crew of the _CCGS Amundsen_, the _Amundsen Science_ program, and the NSERC Ship-Time program (# RGPST-501172-2017). This research was supported by the Sentinel North program of Université Laval, made possible, in part, with funding from the Canada First Research Excellence Fund. MS received support from the Arctic Challenge for Sustainability (ArCS) Project of the Japanese Ministry of Education, Culture, Sports, Science and Technology. **Author contributions:** Conceptualization: AR, LL, MB; Data curation: AR, XX; Formal Analysis: lead by AR, LL, MB, aided by MS, GD, LF; Funding acquisition: MB; Methodology: CM, CP, EL, FD, HC, JL, MB; Visualization: lead by AR, aided by LL, GD; Writing - original draft: AR; Writing - reviewing and editing: all authors. **Competing interests:** The authors declare no competing interests. **Data and materials availability:** Argo float raw data are available through the Argo GDAC ftp site ([ftp://ftp.ifremer.fr/ifremer/argo/](ftp://ftp.ifremer.fr/ifremer/argo/)). Cleaned and quality controlled BGC float and light model data are available at doi:10.5281/zenodo.3944836. The complete workflow to reproduce this study including code and ancillary data is available at doi:10.5281/zenodo.3945047. We thank three anonymous referees for their insightful comments.

**Supplemental Materials**

Fig. S1. Average ratio between the irradiance in modelled and theoretical clear-sky PAR profiles.

Fig. S2. Surface layer extent.

Fig. S3. Vertical backscattering profiles during winter.

Fig. S4. Vertical chlorophyll _a_ fluorescence profiles during winter.

Fig. S5-S12. Computation of net biomass accumulation rates r.

Fig. S13. Vertical density profiles during winter.

Fig. S14. Vertical temperature profiles during winter.

Fig. S15. Vertical salinity profiles during winter.

Fig. S16. Calculating growth rates from smoothed annual cycles.

Fig. S17. Net biomass accumulation rate _r_ for different bin widths.

Fig. S18. Bin width does not significantly affect computation of net biomass accumulation rate _r_ during winter.

Fig. S19. Photosynthetic parameters in Baffin Bay.

Fig. S20-S21. Net growth rates approximately match modelled cell division rates in late winter and early spring.

Fig. S22. Mesozooplankton biomass integrated over the upper 60 m of the water column in the Amundsen Gulf, southwestern Beaufort Sea.

Fig. S23. Mesozooplankton mean biomass integrated over the water column of 20 sampling stations in Baffin Bay and Southeast Beaufort Sea in summer 2016 and 2008, respectively.

Fig. S24. Cloud cover during the winter-spring transition in Baffin Bay.

Fig. S25. Snow cover and snowfall on sea ice during winter and spring 2018 in Baffin Bay.

Table S1. Floats deployed in 2017 and 2018.

Table S2. Sampling schedule of the floats.

Table S3. All sensor correction parameters in this study.

**Figures**

![**Baffin Bay, an Arctic Sea.**  (**A**) Location of Baffin Bay between the Arctic and Atlantic Oceans (inset marking the location of panel B). (**B**) Float trajectories from 2017-19, with winter trajectories interpolated (dotted lines). (**C, D**) Temperature-salinity plots with marginal histograms for two months at a time show a strong seasonality in the upper 50 m (C) and consistent water masses throughout the year in sub-surface layers (50-250 m, D).](../nb_fig/map_hydrography.png){#fig:map_hydrography}

\newpage

![**Annual cycles of phytoplankton biomass.** (**A**) Mixed layer depth (dashed line, individual) and isolume depths for two different irradiance levels. The “surface layer” is defined as the maximum of mixed layer depth and the depth of the 0.4 mol photons m$^{-2}$ d$^{-1}$ isolume for any given profile. X-axis labels indicate the start of each month. (**B, D**) Vertical profiles of two proxies of phytoplankton biomass (B: chlorophyll _a_ fluorescence, chl-a, D: particle backscattering at 700 nm, bbp) show a strong seasonality with rising biomass in winter and spring and a sub-surface maximum in summer. (**C**) Surface-layer integrated chl-a and bbp as observed by four autonomous floats (different markers). (**E, F**) Surface-layer averaged chl-a and bbp. Horizontal bars indicate the 750-800 m depth-averaged background values and their variability ($\pm$ one standard deviation) E: Full time series 2017-19. F: Time series as in E, but collapsed into one annual cycle and smoothed using a Generalized Additive Model (solid lines). (**G**) Net specific growth rates _r_ as calculated from measured chl-a (green tint) and bbp (violet tint), treating each float as a separate time series and afterwards averaged in 28-day bins. Growth rates based on bbp turned positive later in winter because increases are masked by environmental noise, shown in panels E and F. Phytoplankton cell division rates µ (black dots) calculated from measured photosynthetic parameters and each profile's _in situ_ light field. White dots: µ calculated from PAR values at the noise level. (**H**) Net specific growth rates _r_ calculated from the smoothed time series of surface layer biomass, mixed layer depths, and isolume depths. Black curve: Smoothed phytoplankton cell division rates µ.](../nb_fig/biomass_growth.png){#fig:growth}

\newpage

![**Environmental constraints of Arctic phytoplankton throughout the year.** (A) Ocean-atmosphere heat flux. Positive values indicate energy leaving the ocean, leading to loss of buoyancy and vigorous mixing. (B) Sea ice concentration, averaged across the study area. (C) Sun elevation angle. During the polar night, the sun does not rise for two months.](../nb_fig/fig_environment.png){#fig:environment}

\newpage

**References and Notes**
