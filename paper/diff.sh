#!/bin/bash

tmpdir=_tmp_manuscript_diff

# set only if not already defined
: "${bib:=bibliographyfull.bib}"

git clone -b v0.2 .. $tmpdir

# parse papers both in old and new versions
articlefile='paper'

# old
cd $tmpdir/paper
old=$tmpdir/paper/${articlefile}.md
cd ../..
# new
new=${articlefile}.md

tracked=${articlefile}_trackchanges

pandiff $old $new -o $tracked.html --bibliography $bib --filter pandoc-crossref  --filter pandoc-citeproc --csl $cslfile --mathjax

# append stylesheet
echo '
<style>
del {
  color: #b31d28;
  background-color: #ffeef0;
  text-decoration: line-through;
}
ins {
  color: #22863a;
  background-color: #f0fff4;
  text-decoration: underline;
}
img {
  max-width: 100%;
  min-width: 60%;
}
</style>
' >> $tracked.html

rm -rf $tmpdir
