---
title: "Supplementary Materials for: Arctic mid-winter phytoplankton growth revealed by autonomous profilers"
header-includes: |
  \usepackage{float}
  \makeatletter
  \def\fps@figure{H}
  \def\fps@table{H}
  \makeatother
  \renewcommand{\thefigure}{S\arabic{figure}}
  \renewcommand{\thetable}{S\arabic{table}}
geometry: margin=1.5cm
figureTemplate: $$figureTitle$$ S$$i$$$$titleDelim$$ $$t$$
tableTemplate: $$tableTitle$$ $$i$$$$titleDelim$$ $$t$$
---

**Authors**: Achim Randelhoff*, Léo Lacour, Claudie Marec, Edouard Leymarie, José Lagunas, Xiaogang Xing, Gérald Darnis, Christophe Penkerc'h, Makoto Sampei, Louis Fortier, Fabrizio D'Ortenzio, Hervé Claustre, Marcel Babin

\* Correspondence to: achim.randelhoff@takuvik.ulaval.ca

# This PDF includes:

Figures S1-S25

Tables S1-S3

# Figures S1 to S25


![**Average ratio between the irradiance in modelled and theoretical clear-sky PAR profiles.**](../nb_fig/Correction_factor.png){#fig:timeseries_correction_factor}

\newpage

![**Surface layer extent.** The depth of the surface layer was defined, for each profile, as either the depth of the mixed layer or the depth of the 0.4 isolume, whichever was deeper.](../nb_fig/timeseries_surfacelayer.png){#fig:timeseries_surfacelayer}

\newpage

![**Vertical backscattering profiles during winter.** Columns correspond to months January (1) through May (5), rows to one float and one year. The black dots indicate mixed layer depth for each profile. Also see the supplementary html files that allow interactively browsing all vertical profiles in all months for all floats.](../nb_fig/profiles_Corbbp.png){#fig:profiles_Corbbp}

\newpage

![**Vertical chlorophyll _a_ fluorescence profiles during winter.** Columns correspond to months January (1) through May (5), rows to one float and one year. The black dots indicate mixed layer depth for each profile. Also see the supplementary html files that allow interactively browsing all vertical profiles in all months for all floats.](../nb_fig/profiles_CorFChla.png){#fig:profiles_CorFChla}

\newpage

![**Computation of net biomass accumulation rate r for float 012b from chlorophyll _a_ fluorescence.** (A) Deepening (positive values) and shoaling (negative values) of the surface mixed layer. (B) Whether the mixed layer was deeper or shallower than the 0.4 mol m$^{-2}$ d$^{-1}$ isolume. (C) Whether mixed layer chl-a was significantly larger than chl-a in the layer down to 15 m below. (D) chl-a integrated over the surface layer depth. (E) chl-a averaged over the surface layer depth. (F) Specific biomass change calculated from surface layer chl-a. Wherever all the conditions in panels A-C are all fulfilled, the growth rate (panel F) is calculated from integrated values (panel D), where it is not, from averaged values (panel E). When two successive average chl-a values indicate that both are not different from the background noise signal (marked in panel E), the corresponding growth rate is not considered significant (triangles in panel F). All time series here shown based on values averaged in 14-day bins.](../nb_fig/growth_rate_calculation_criteria_012b_chla.png){#fig:growth_rate_calculation_criteria_012b_chla height=80%}

\newpage

![**Like [@Fig:growth_rate_calculation_criteria_012b_chla] but for float 016b.**](../nb_fig/growth_rate_calculation_criteria_016b_chla.png){#fig:growth_rate_calculation_criteria_016b_chla}

\newpage

![**Like [@Fig:growth_rate_calculation_criteria_012b_chla] but for float 017b.**](../nb_fig/growth_rate_calculation_criteria_017b_chla.png){#fig:growth_rate_calculation_criteria_017b_chla}

\newpage

![**Like [@Fig:growth_rate_calculation_criteria_012b_chla] but for float 020b.**](../nb_fig/growth_rate_calculation_criteria_020b_chla.png){#fig:growth_rate_calculation_criteria_020b_chla}

\newpage

![**Like [@Fig:growth_rate_calculation_criteria_012b_chla] but based on backscattering (bbp) instead of chlorophyll _a_ fluorescence.**](../nb_fig/growth_rate_calculation_criteria_012b_bbp.png){#fig:growth_rate_calculation_criteria_012b_bbp}

\newpage

![**Like [@Fig:growth_rate_calculation_criteria_012b_bbp] but for float 016b.**](../nb_fig/growth_rate_calculation_criteria_016b_bbp.png){#fig:growth_rate_calculation_criteria_016b_bbp}

\newpage

![**Like [@Fig:growth_rate_calculation_criteria_012b_bbp] but for float 017b.**](../nb_fig/growth_rate_calculation_criteria_017b_bbp.png){#fig:growth_rate_calculation_criteria_017b_bbp}

\newpage

![**Like [@Fig:growth_rate_calculation_criteria_012b_bbp] but for float 020b.**](../nb_fig/growth_rate_calculation_criteria_020b_bbp.png){#fig:growth_rate_calculation_criteria_020b_bbp}

\newpage

![**Vertical density profiles during winter.** Columns correspond to months January (1) through May (5), rows to one float and one year. The black dots indicate mixed layer depth for each profile. Also see the supplementary html files that allow interactively browsing all vertical profiles in all months for all floats.](../nb_fig/profiles_Density.png){#fig:profiles_Density}

\newpage

![**Vertical temperature profiles during winter.** Columns correspond to months January (1) through May (5), rows to one float and one year. The black dots indicate mixed layer depth for each profile. Also see the supplementary html files that allow interactively browsing all vertical profiles in all months for all floats.](../nb_fig/profiles_Temperature.png){#fig:profiles_Temperature}

\newpage

![**Vertical salinity profiles during winter.** Columns correspond to months January (1) through May (5), rows to one float and one year. The black dots indicate mixed layer depth for each profile. Also see the supplementary html files that allow interactively browsing all vertical profiles in all months for all floats.](../nb_fig/profiles_Salinity.png){#fig:profiles_Salinity}

\newpage

![**Calculating growth rates from smoothed annual cycles.** Quantities relevant for calculation of growth rates, here shown using a Generalized Additive Model with 12 cubic splines over the annual cycle. The jumps in growth rates are at times when the criteria changed to calculate growth rate from either mean or integrated biomasses. E.g. in late September, the mixed layer deepened below the 0.4 isolume depth. From late January to early May, overall smoothed bbp-based biomass was not significantly larger than the noise level; therefore, no growth rates were computed during that time from the smoothed time series.](../nb_fig/growth_rate_calculation_smoothed.png){#fig:growth_rate_calculation_smoothed height=80%}

\newpage

![**Net biomass accumulation rate r for different bin widths.** Computed specific net growth rates from each of the four floats for four selected bin widths.](../nb_fig/growth_rate_calculation_vary_bin_widths.png){#fig:growth_rate_calculation_vary_bin_widths}

\newpage

![**Bin width does not significantly affect computation of net biomass accumulation rate r during winter.** Two selected winter periods corresponding to those reported in the main paper show no significant differences between two bin widths; we hence choose 14 days. Upper right corner: A Kruskal-Wallis test returned non-significant differences between the two groups.](../nb_fig/growth_rate_calculation_bin_width_robustness.png){#fig:growth_rate_calculation_bin_width_robustness}

\newpage

![**Photosynthetic parameters in Baffin Bay.** (A) $E_k$, the light saturation parameter for photosynthesis, and (B) P$_\mathrm{b, max}$, the chlorophyll _a_ specific light saturated growth rate. The black line marks averaged values as used for modelling cell division rates.](../nb_fig/PE_values.png){#fig:PE_values}

\newpage

![**Net growth rates approximately match modelled cell division rates in late winter and early spring.** The data represent monthly averages averaged over all floats; each number indicates the month of the year.](../nb_fig/growth_rate_mu_comparison.png){#fig:growth_rate_mu_comparison}

\newpage

![**Net growth rates approximately match modelled cell division rates in late winter and early spring.** The data represent monthly averages averaged for each of the 4 floats; each number indicates the month of the year.](../nb_fig/growth_rate_mu_comparison_per_float.png){#fig:growth_rate_mu_comparison_per_float height=90%}

\newpage

![Mesozooplankton biomass integrated over the upper 60 m of the water column in the Amundsen Gulf, southwestern Beaufort Sea. The dotted line denotes the split between winter and summer in the  bar plot averages.](../nb_fig/timeseries_zoo.png){#fig:timeseries_zoo}

\newpage

![Mesozooplankton mean biomass integrated over the water column of 20 sampling stations in Baffin Bay and Southeast Beaufort Sea in summer 2016 and 2008, respectively.](../misc_fig/zooplankton_baffin_vs_amundsen.png){#fig:zoo_baffin_amundsen_comparison}

\newpage

![Cloud cover during the winter-spring transition in Baffin Bay.](../nb_fig/timeseries_clouds.png){#fig:clouds}

\newpage

![**Snow cover and snowfall on sea ice during winter and spring 2018 in Baffin Bay.** A, B: ERA-Interim precipitation converted to snowfall using a theoretical snow density of 200 kg m$^{-3}$. C: Snow cover observed by satellite.](../nb_fig/timeseries_snow.png){#fig:snow}

\newpage

# Tables S1 to S3

Season|WMO number|Float name|Fate
---|---|---|---
2017-2018|||
\hphantom|4901804|takapm006b|Lost immediately after deployment
\hphantom|6902666|takapm007b|Lost in fall 2017
\hphantom|6902669|takapm008b|Lost during winter
\hphantom|4901805|takapm012b|Survived winter; recovered during ISP 2018
\hphantom|6902670|takapm015b|Lost during winter
\hphantom|6902671|takapm016b|Survived two winters; changed WMO after firmware update
\hphantom|6902829|takapm017b|Stopped sampling in April 2018; recovered in July 2018 from the CCGS Amundsen
2018-2019|||
\hphantom|6902896|takapm011b|Survived winter but emerged south of Davis Strait in summer 2019
\hphantom|6902953|takapm016b|From 2018-11-1 (changed WMO number after firmware update); survived second winter
\hphantom|6902897|takapm020b|Survived winter

: Floats deployed in 2017 and 2018. {#tbl:floats}

\newpage

\hphantom|16 November through June|July|August-October|1 to 15 November*
---|---|---|---|---|
Sampling interval (days)|28\**|10|1|1 or 28\**
Profiles up to|10 m|10 m|surface|surface or 10 m

: Sampling schedule of the floats. \*Depending on ice cover. \** 14 days for float takapm020b {#tbl:schedule}

\newpage

Float Name|WMO|Optode slope|FChla Dark|FChla F-factor
---|---|---|---|---
takapm012b|4901805|1.1009|0.036 |0.6598
takapm016b|6902071|1.1375|0.0511|0.4886
takapm016b|6902953\*|1.1479|0.0438|0.756
takapm017b|6902829|1.1508|0.0511|0.6601
takapm020b|6902897|1.0454|0.0365|0.603

: All sensor correction parameters in this study. \*New WMO number after firmware update {#tbl:correction_factors}
