#!/bin/bash

export cslfile='template/science-corrected.csl'

cd paper # ./paper

for file in 'paper'
do
  export articlefile=$file
  export refdoc=template/reference_${articlefile}.docx
  #
  pandoc --filter pandoc-crossref --filter pandoc-citeproc --csl $cslfile --lua-filter=pagebreak.lua --reference-doc $refdoc --mathjax -o ${articlefile}.docx ${articlefile}.md bibliography.yaml

done

pandoc --filter pandoc-crossref -o supmat.pdf supmat.md

cd .. # ./
